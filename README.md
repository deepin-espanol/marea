# Marea
Marea (AKA "Marea-cinnamon") is a set of icons developed by Alejandro and Álvaro for Deepin. It is currently being administered by Deepin in Spanish.

## Download
Download them in [Deepin en Español](https://deepin-espanol.github.io/apps/iconos-marea/). Original sources is from [Seducción Linux](https://seduccionlinux.wordpress.com/2017/09/10/marea-icons-otros-tremendos-iconos-para-deepin/).

## License
Marea set icons is licensed under GNU Public license, version 3.0. Some application icons are trademarks of their respective owners.
